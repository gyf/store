/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/
package bigfilterdb

import (
	"chainmaker.org/chainmaker/store/v2/serialization"
)

// BigFilterDB provides handle to block and tx instances
type BigFilterDB interface {
	// InitGenesis 初始化，写入一个创世区块:0号区块
	InitGenesis(genesisBlock *serialization.BlockWithSerializedInfo) error

	// CommitBlock commits the txId and savepoint in an atomic operation
	CommitBlock(blockWithRWSet *serialization.BlockWithSerializedInfo, isCache bool) error

	// GetLastSavepoint returns the last block height
	GetLastSavepoint() (uint64, error)

	// TxExists if not exists  returns (false,false,nil) . if exists return (true,_,nil).
	// If probably exists return (false,true,nil)
	TxExists(txId string) (bool, bool, error)

	// Close is used to close database
	Close()
}
