package filter

// Filter support add key , check key is/not exist
type Filter interface {
	// Add a key to filter
	Add(key []byte) error

	// Check more keys is/not exist
	ExistMult(arr [][]byte) []bool

	// Check key is/not exist
	Exist(key []byte) (bool, error)

	// Add more key to filter
	AddMult(arr [][]byte) error

	// Save last point to server
	SaveLastPoint(pointK string, pointV string) error

	// get last point to server
	GetLastPoint(pointK string) ([]string, error)

	//Dump(b []byte) error
	//
	//Recover()
}
