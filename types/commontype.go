/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package types

type SavePoint struct {
	BlockHeight uint64 `gorm:"primarykey"`
}

// 获得创建table的ddl语句
func (b *SavePoint) GetCreateTableSql(dbType string) string {
	if dbType == "mysql" {
		return "CREATE TABLE `save_points` (`block_height` bigint unsigned AUTO_INCREMENT,PRIMARY KEY (`block_height`))"
	} else if dbType == "sqlite" {
		return "CREATE TABLE `save_points` (`block_height` integer,PRIMARY KEY (`block_height`))"
	}
	panic("Unsupported db type:" + dbType)
}

// 获得table name
func (b *SavePoint) GetTableName() string {
	return "save_points"
}

// 获得 insert sql 语句
func (b *SavePoint) GetInsertSql(dbType string) (string, []interface{}) {
	return "INSERT INTO save_points values(?)", []interface{}{b.BlockHeight}
}

// 获得 update sql 语句
func (b *SavePoint) GetUpdateSql() (string, []interface{}) {
	return "UPDATE save_points set block_height=?", []interface{}{b.BlockHeight}
}

// 获得 得到数据总行数 sql 语句
func (b *SavePoint) GetCountSql() (string, []interface{}) {
	return "SELECT count(*) FROM save_points", []interface{}{}
}
