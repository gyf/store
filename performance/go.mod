module chainmaker.org/chainmaker/store/performance

go 1.15

require (
	chainmaker.org/chainmaker/localconf/v2 v2.1.1-0.20211214124610-bb7620382194
	chainmaker.org/chainmaker/logger/v2 v2.1.1-0.20211214124250-621f11b35ab0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1-0.20220325091111-7b6ff1727aa8
	chainmaker.org/chainmaker/protocol/v2 v2.2.1-0.20220322035229-9472df51ccd4
	chainmaker.org/chainmaker/store/v2 v2.1.1
	chainmaker.org/chainmaker/utils/v2 v2.2.0
	github.com/spf13/cobra v1.1.1
)

replace (
	chainmaker.org/chainmaker/store/v2 => ../
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
)
