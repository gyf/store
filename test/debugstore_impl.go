/*
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package test

import (
	"errors"
	"fmt"
	"time"

	acPb "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/store/v2/conf"
)

var (
	errNotFound = errors.New("not found")
)

//DebugStore 本实现是用于避开存储模块的内存、CPU和IO的影响的情况下，提供最模拟的基本区块写入，读取等。不能安装合约，写入状态。
type DebugStore struct {
	logger      protocol.Logger
	storeConfig *conf.StorageConfig
	dbHandle    protocol.DBHandle
}

// 根据txid得到 交易info
func (d *DebugStore) GetTxWithInfo(txId string) (*commonPb.TransactionInfo, error) {
	//TODO implement me
	panic("implement me")
}

// 根据txid 得到交易info
func (d *DebugStore) GetTxInfoOnly(txId string) (*commonPb.TransactionInfo, error) {
	//TODO implement me
	panic("implement me")
}

//GetTxWithRWSet return tx and it's rw set
func (d *DebugStore) GetTxWithRWSet(txId string) (*commonPb.TransactionWithRWSet, error) {
	panic("implement me")
}

//GetTxInfoWithRWSet return tx and tx info and rw set
func (d *DebugStore) GetTxInfoWithRWSet(txId string) (*commonPb.TransactionInfoWithRWSet, error) {
	panic("implement me")
}
func NewDebugStore(l protocol.Logger, config *conf.StorageConfig, db protocol.DBHandle) *DebugStore {
	return &DebugStore{
		logger:      l,
		storeConfig: config,
		dbHandle:    db,
	}
}

//根据合约，sql,value得到对应行数据
func (d *DebugStore) QuerySingle(contractName, sql string, values ...interface{}) (protocol.SqlRow, error) {
	d.logger.Panic("implement me")
	panic("implement me")
}

//根据合约，sql,value得到对应 多行数据
func (d *DebugStore) QueryMulti(contractName, sql string, values ...interface{}) (protocol.SqlRows, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//执行合约sql
func (d *DebugStore) ExecDdlSql(contractName, sql, version string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

//开启事务
func (d *DebugStore) BeginDbTransaction(txName string) (protocol.SqlDBTransaction, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//得到当前事务
func (d *DebugStore) GetDbTransaction(txName string) (protocol.SqlDBTransaction, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//提交事务
func (d *DebugStore) CommitDbTransaction(txName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

//回滚事务
func (d *DebugStore) RollbackDbTransaction(txName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

//创建database
func (d *DebugStore) CreateDatabase(contractName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

//删除database
func (d *DebugStore) DropDatabase(contractName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

//得到合约对应database
func (d *DebugStore) GetContractDbName(contractName string) string {
	d.logger.Panic("not implement")
	panic("implement me")
}

//判断交易是否存在
func (d *DebugStore) TxExistsInFullDB(txId string) (bool, uint64, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//增量防重
func (d *DebugStore) TxExistsInIncrementDB(txId string, startHeight uint64) (bool, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//得到合约
func (d *DebugStore) GetContractByName(name string) (*commonPb.Contract, error) {
	return nil, errNotFound
}

//得到合约字节码
func (d *DebugStore) GetContractBytecode(name string) ([]byte, error) {
	return nil, errNotFound
}

//得到member
func (d *DebugStore) GetMemberExtraData(member *acPb.Member) (*acPb.MemberExtraData, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//创世块初始化写入
func (d *DebugStore) InitGenesis(genesisBlock *storePb.BlockWithRWSet) error {
	//put block header
	block := genesisBlock.Block
	header, _ := block.Header.Marshal()
	err := d.dbHandle.Put(append([]byte("hash_"), block.Header.BlockHash...), header)
	if err != nil {
		return err
	}
	err = d.dbHandle.Put([]byte("lastBlockHeight"), header)
	if err != nil {
		return err
	}
	err = d.dbHandle.Put([]byte(fmt.Sprintf("height_%d", block.Header.BlockHeight)), header)
	if err != nil {
		return err
	}
	for _, rw := range genesisBlock.TxRWSets {
		for _, w := range rw.TxWrites {
			err = d.dbHandle.Put(append([]byte(w.ContractName+"#"), w.Key...), w.Value)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

//写区块和读写集
func (d *DebugStore) PutBlock(block *commonPb.Block, txRWSets []*commonPb.TxRWSet) error {
	d.logger.Infof("mock put block[%d] tx count:%d", block.Header.BlockHeight, len(block.Txs))
	startTime := time.Now()
	defer func() {
		d.logger.Infof("put block[%d] cost time:%s", block.Header.BlockHeight, time.Since(startTime))
	}()
	//put block header
	header, _ := block.Header.Marshal()
	err := d.dbHandle.Put(append([]byte("hash_"), block.Header.BlockHash...), header)
	if err != nil {
		return err
	}
	err = d.dbHandle.Put([]byte("lastBlockHeight"), header)
	if err != nil {
		return err
	}
	return d.dbHandle.Put([]byte(fmt.Sprintf("height_%d", block.Header.BlockHeight)), header)
}

//根据hash得到block
func (d *DebugStore) GetBlockByHash(blockHash []byte) (*commonPb.Block, error) {
	data, err := d.dbHandle.Get(append([]byte("hash_"), blockHash...))
	if err != nil {
		return nil, err
	}
	header := &commonPb.BlockHeader{}
	err = header.Unmarshal(data)
	if err != nil {
		return nil, err
	}
	return &commonPb.Block{Header: header}, nil
}

//根据hash 判断区块是否存在
func (d *DebugStore) BlockExists(blockHash []byte) (bool, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//根据hash得到块高
func (d *DebugStore) GetHeightByHash(blockHash []byte) (uint64, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//根据块高得到区块header
func (d *DebugStore) GetBlockHeaderByHeight(height uint64) (*commonPb.BlockHeader, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//根据区块块高，得到区块
func (d *DebugStore) GetBlock(height uint64) (*commonPb.Block, error) {
	data, err := d.dbHandle.Get([]byte(fmt.Sprintf("height_%d", height)))
	if err != nil {
		return nil, err
	}
	header := &commonPb.BlockHeader{}
	err = header.Unmarshal(data)
	if err != nil {
		return nil, err
	}
	return &commonPb.Block{Header: header}, nil
}

//得到最后的配置区块
func (d *DebugStore) GetLastConfigBlock() (*commonPb.Block, error) {
	return d.GetBlock(0)
}

//得到链配置
func (d *DebugStore) GetLastChainConfig() (*configPb.ChainConfig, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//通过txid得到区块
func (d *DebugStore) GetBlockByTx(txId string) (*commonPb.Block, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//通过height得到区块和读写集
func (d *DebugStore) GetBlockWithRWSets(height uint64) (*storePb.BlockWithRWSet, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//根据txid得到交易
func (d *DebugStore) GetTx(txId string) (*commonPb.Transaction, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//交易过滤，判断是否存在
func (d *DebugStore) TxExists(txId string) (bool, error) {
	return false, nil
}

//根据txid返回块高
func (d *DebugStore) GetTxHeight(txId string) (uint64, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//根据txid返回确认时间
func (d *DebugStore) GetTxConfirmedTime(txId string) (int64, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//得到最后写入区块
func (d *DebugStore) GetLastBlock() (*commonPb.Block, error) {
	key := []byte("lastBlockHeight")
	data, err := d.dbHandle.Get(key)
	if err != nil || len(data) == 0 {
		return nil, errNotFound
	}
	header := &commonPb.BlockHeader{}
	err = header.Unmarshal(data)
	if err != nil {
		return nil, err
	}
	return &commonPb.Block{Header: header}, nil
}

//读合约key对应value
func (d *DebugStore) ReadObject(contractName string, key []byte) ([]byte, error) {
	dbKey := append([]byte(contractName+"#"), key...)
	return d.dbHandle.Get(dbKey)
}

//合约范围查询
func (d *DebugStore) SelectObject(contractName string, startKey []byte, limit []byte) (protocol.StateIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//根据txid得到读写集
func (d *DebugStore) GetTxRWSet(txId string) (*commonPb.TxRWSet, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//根据块高，返回读写集
func (d *DebugStore) GetTxRWSetsByHeight(height uint64) ([]*commonPb.TxRWSet, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetDBHandle(dbName string) protocol.DBHandle {
	return d.dbHandle
}

func (d *DebugStore) GetArchivedPivot() uint64 {
	return 0
}

//区块归档
func (d *DebugStore) ArchiveBlock(archiveHeight uint64) error {
	return nil
}

//转储区块
func (d *DebugStore) RestoreBlocks(serializedBlocks [][]byte) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

//关闭db
func (d *DebugStore) Close() error {
	return d.dbHandle.Close()
}

//得到历史数据
func (d *DebugStore) GetHistoryForKey(contractName string, key []byte) (protocol.KeyHistoryIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//得到账户历史数据
func (d *DebugStore) GetAccountTxHistory(accountId []byte) (protocol.TxHistoryIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

//得到合约历史数据
func (d *DebugStore) GetContractTxHistory(contractName string) (protocol.TxHistoryIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}
