VERSION221=v2.2.1
VERSION222=v2.2.2
gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/pb-go/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION222)
	go get chainmaker.org/chainmaker/store-badgerdb/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/store-leveldb/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/store-sqldb/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/store-tikv/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/utils/v2@$(VERSION222)
	go mod tidy
ut:
	go test ./...
	(gocloc --include-lang=Go --output-type=json . | jq '(.total.comment-.total.files*6)/(.total.code+.total.comment)*100')