/*
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package rolling_window_cache

import "chainmaker.org/chainmaker/store/v2/serialization"

// 一个滑动窗口Cache
type RollingWindowCache interface {
	// 判断 key 在 start >r.startBlockHeight条件下，在 [start,r.endBlockHeight]区间内 是否存在
	// return: bool start是否在滑动窗口内， bool key是否存在， error 返回err
	Has(key string, start uint64) (bool, bool, error)
	//消费 chan 中 数据到 RollingWindowCache
	Consumer()
	// InitGenesis commit genesis block
	InitGenesis(genesisBlock *serialization.BlockWithSerializedInfo) error
	// CommitBlock commits the txId and savepoint in an atomic operation
	CommitBlock(blockWithRWSet *serialization.BlockWithSerializedInfo, isCache bool) error
	// ResetRWCache set RollingWindowCache use blockInfo
	ResetRWCache(blockInfo *serialization.BlockWithSerializedInfo) error
}
