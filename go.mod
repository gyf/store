module chainmaker.org/chainmaker/store/v2

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	chainmaker.org/chainmaker/store-badgerdb/v2 v2.2.1
	chainmaker.org/chainmaker/store-leveldb/v2 v2.2.1
	chainmaker.org/chainmaker/store-sqldb/v2 v2.2.1
	chainmaker.org/chainmaker/store-tikv/v2 v2.2.1
	chainmaker.org/chainmaker/utils/v2 v2.2.2
	github.com/allegro/bigcache/v3 v3.0.2
	github.com/gogo/protobuf v1.3.2
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
	github.com/golang/mock v1.6.0
	github.com/google/flatbuffers v2.0.0+incompatible // indirect
	github.com/longxibendi/redisbloom-go v1.1.0
	github.com/mitchellh/mapstructure v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/spaolacci/murmur3 v1.1.0
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.1-0.20200815110645-5c35d600f0ca // indirect
	github.com/tidwall/tinylru v1.1.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/text v0.3.7 // indirect
)

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0
